import { INVALID_MOVE } from 'boardgame.io/core';

var plateau = [
    0,0,0,0,1,0,
    0,0,3,0,0,0,
    1,3,0,3,0,0,
    0,1,0,2,1,1,
    2,0,3,0,0,2,
    0,0,0,0,0,0
    ];

function shuffle(t){
    const l = t.length;
    var tcopy = Array.from(t);
    var ans = [];
    for(let i=l;i>0;i--){
        var idx =  Math.floor(Math.random()*i);
        ans.push(tcopy.splice(idx,1)[0]);
    }
    return ans;
}



function emptyLine(cells,l){
    return cells.slice(6*l,6*(l+1)).every((c)=>c==0)
}
function emptyLineStart(cells,side){
    return cells[side].every((c)=>c==0)
}

function nghbr(i){
    var ans = [];
    if (i>5){
        ans.push(i-6);
    }
    else {
        ans.push('south');
    }
    if (i<30){
        ans.push(i+6);
    }
    else{
        ans.push("north");
    }
    if(i%6!=0){
        ans.push(i-1);
    }
    if(i%6!=5){
        ans.push(i+1);
    }
    return ans;
}


function pathsFrom(cells,fr){
    var workingCells = Array.from(cells);
    var count = workingCells[fr];
    var to = new Set();
    var path = [];
    workingCells[fr] = 0;
    //var current = fr;
    var evolvingPath = {
        path:path,
        cell:fr,
        count:count
    };
    //console.log(evolvingPath)
    var queue = [evolvingPath];
    while (queue.length>0){
        //console.log(queue)
        var current = queue.pop();
        /*var path = current[0];
        var cell = current[1];
        var count = current[2];*/
        if(typeof(current.cell)=="string"){
            if(current.count==0){
                to.add(current.cell);
                //console.log("paths size",to.size);
            }
        }
        else{
            if (current.count == 0 && workingCells[current.cell]==0){
                to.add(current.cell);
            }
            else{
                if (current.count == 0){
                    current.count = workingCells[current.cell];
                }
                var neighboors = nghbr(current.cell);
                for(var n of neighboors){
                    var dir = {};
                    if(current.cell<n){
                        dir = {lo : current.cell,up: n};
                    }
                    else {
                        dir = {lo : n,up: current.cell};
                    }
                    if (current.path.filter((p)=>p.lo==dir.lo && p.up == dir.up).length==0 
                        && (workingCells[n]==0 || current.count == 1)){
                        var newpath = Array.from(current.path);
                        newpath.push(dir);
                        queue.push({
                            path:newpath,
                            cell:n,
                            count: current.count-1
                        });
                    } 
                }
            }
        }
    }
//removing duplicate
//console.log(cells,to);
return to;
}

function pathsBetween(cells,beginning,end){
    var workingCells = Array.from(cells);
    var count = workingCells[beginning];
    var to = new Set();
    var path = [];
    workingCells[beginning] = 0;
    //var current = fr;
    var evolvingPath = {
        path:path,
        cell:beginning,
        count:count
    };
    //console.log(evolvingPath)
    var queue = [evolvingPath];
    while (queue.length>0){
        //console.log(queue)
        var current = queue.pop();
        /*var path = current[0];
        var cell = current[1];
        var count = current[2];*/
        if(typeof(current.cell)=="string"){
            if(current.count==0){
                if(current.cell === end){
                return current.path;
                }
                //to.add(current.cell);
                //console.log("paths size",to.size);
            }
        }
        else{
            if (current.count == 0 && workingCells[current.cell]==0){
                if(current.cell === end){
                    return current.path;
                    }
                //to.add(current.cell);
            }
            else{
                if (current.count == 0){
                    current.count = workingCells[current.cell];
                }
                var neighboors = nghbr(current.cell);
                for(var n of neighboors){
                    var dir = {};
                    if(current.cell<n){
                        dir = {lo : current.cell,up: n};
                    }
                    else {
                        dir = {lo : n,up: current.cell};
                    }
                    if (current.path.filter((p)=>p.lo==dir.lo && p.up == dir.up).length==0 
                        && (workingCells[n]==0 || current.count == 1)){
                        var newpath = Array.from(current.path);
                        newpath.push(dir);
                        queue.push({
                            path:newpath,
                            cell:n,
                            count: current.count-1
                        });
                    } 
                }
            }
        }
    }
//removing duplicate
//console.log(cells,to);
return null;
}

function Place (G,ctx,side,fr,to)  {
    console.log("Place",side,fr,to)
    if (side.toString() !== ctx.currentPlayer) {
        return INVALID_MOVE;
    }
    else {
        G.cells[to]=G.start[side][fr];
        G.start[side][fr]=0;
    }
}

function RandomStart(G,ctx,side) {
    console.log("RandomStart:",side)
    if (side.toString() !== ctx.currentPlayer) {
        return INVALID_MOVE;
    }
    else {
        let shuffled = shuffle([1,1,2,2,3,3]);
        for(let i = 0;i<6;i++){
        G.cells[30*side+i]=shuffled[i];
        G.start[side][i]=0;
        }
    }
}

function RegularMove (G, ctx, fr,to) {
    var line = Math.floor(fr/6);
    var incr = ctx.currentPlayer*2-1;
    console.log(line,incr);
    for(var l=line+incr;l>=0 && l<=5;l=l+incr){
        if (!emptyLine(G.cells,l)){return INVALID_MOVE;}
    }
    
    if (pathsBetween(G.cells,fr,to)){
        var piece = G.cells[fr];
        G.cells[fr] = 0;
        if(typeof(to)==="string"){
            G[to] = piece;
        }
        else{
            G.cells[to] = piece;
        }
    }
    else {
        return INVALID_MOVE;
    }
}

//console.log(pathsFrom(plateau,26))

const Gyges = {
    setup: () => ({ cells: Array(36).fill(0),
        north: 0,
        south: 0,
        start: [[1,1,2,2,3,3],[1,1,2,2,3,3]],
        firstClick: null,
         }),
    
    turn: {
        moveLimit: 1,
      },
    
    phases:{
        init:{
            moves: {
                Place,
                RandomStart,
            },
            turn: {
                moveLimit: 1,
                order: {
                    first: (G,ctx) => 0,
                    next: (G,ctx) => {
                        var nextPlayer = (ctx.playOrderPos + 1) % ctx.numPlayers;
                        console.log('nextPlayer before text',nextPlayer)
                        if(emptyLineStart(G.start,nextPlayer)){
                            console.log("emptyLine of side:",nextPlayer);
                            nextPlayer = (nextPlayer + 1) % ctx.numPlayers;
                            console.log('nextplayer',nextPlayer)
                        }
                        return nextPlayer;
                    },
                },
            },
            start: true,
            next: 'play',
            endIf: G => (emptyLineStart(G.start,0)&&emptyLineStart(G.start,1))
        },
        play: {
            moves: {RegularMove},
            endIf: (G,ctx) => {
                if(ctx.currentPlayer === "0" && G['north']!==0){
                    return { winner: ctx.currentPlayer };
                }
                if(ctx.currentPlayer === "1" && G['south']!==0){
                    return { winner: ctx.currentPlayer };
                }
            },
            turn: {
                moveLimit: 1,
            },
    }
},
ai: {
    enumerate: (G, ctx) => {
      let moves = [];
      //searching lines of start
      let pl = Number(ctx.currentPlayer);
      let incr = 1-2*pl;
      var startLine = pl*5;
      if(ctx.phase === "init"){
        for(let i=0;i<6;i++){
            if(G.start[pl][i]!==0){
                for(let j=0;j<6;j++){
                    if(G.cells[30*pl+j]===0){
                        moves.push({move:'Place',args:[pl,i,30*pl+j]})
                    }
                }
                
            }
        }
      }
      else{
        while(emptyLine(G.cells,startLine)){startLine+=incr;}
        for (let i = 0; i < 6; i++) {
            if (G.cells[6*startLine+i] !== 0) {
                let to = pathsFrom(G.cells,6*startLine+i); 
                Array.prototype.push.apply(
                    moves,
                    Array.from(to).map((e)=>
                        ({
                            move:'RegularMove',
                            args:[6*startLine+i,e],
                        }))
                );
            }
        }
      }
      //console.log("AI moves list:",moves.length, moves)
      return moves;
    },
  },


    

    
  };

export {Gyges}
