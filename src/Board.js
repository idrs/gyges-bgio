
import React from 'react';

function appartenance(set,clicked,id){
  var classes = '';
  var sep = "";
//  console.log("appartenance",set)
  if(set.has(id)){
    classes += "arrivalsCell";
    sep = " "
  }
  if(clicked===id){
    classes += sep + "selectedCell";
  }
  return classes;
}

function nghbr(i){
  var ans = [];
  if (i>5){
      ans.push(i-6);
  }
  else {
      ans.push('south');
  }
  if (i<30){
      ans.push(i+6);
  }
  else{
      ans.push("north");
  }
  if(i%6!=0){
      ans.push(i-1);
  }
  if(i%6!=5){
      ans.push(i+1);
  }
  return ans;
}


function pathsFrom(cells,fr){
  var workingCells = Array.from(cells);
  var count = workingCells[fr];
  var to = new Set();
  var path = [];
  workingCells[fr] = 0;
  //var current = fr;
  var evolvingPath = {
      path:path,
      cell:fr,
      count:count
  };
  //console.log(evolvingPath)
  var queue = [evolvingPath];
  while (queue.length>0){
      //console.log(queue)
      var current = queue.pop();
      /*var path = current[0];
      var cell = current[1];
      var count = current[2];*/
      if(typeof(current.cell)=="string"){
          if(current.count==0){
              to.add(current.cell);
              //console.log("paths size",to.size);
          }
      }
      else{
          if (current.count == 0 && workingCells[current.cell]==0){
              to.add(current.cell);
          }
          else{
              if (current.count == 0){
                  current.count = workingCells[current.cell];
              }
              var neighboors = nghbr(current.cell);
              for(var n of neighboors){
                  var dir = {};
                  if(current.cell<n){
                      dir = {lo : current.cell,up: n};
                  }
                  else {
                      dir = {lo : n,up: current.cell};
                  }
                  if (current.path.filter((p)=>p.lo==dir.lo && p.up == dir.up).length==0 
                      && (workingCells[n]==0 || current.count == 1)){
                      var newpath = Array.from(current.path);
                      newpath.push(dir);
                      queue.push({
                          path:newpath,
                          cell:n,
                          count: current.count-1
                      });
                  } 
              }
          }
      }
  }
//removing duplicate
console.log(cells,to);
return to;
}

function drawCoin(s) {
  let circles = []
  for(let i=0;i<s;i++){
    circles.push(<circle cx="24" cy="24" r={22-6*i} stroke="black" fill="white" stroke-width="2" /> );
  }
  return(<svg height="48" width="48">{circles}</svg>);
}

//let firstClick = null;

export class GygesBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstClick: null,
      side: null,
      arrivals: new Set(),
      arrivalsChecked: true,
    };  }

  toggleArrivals() {
    this.setState(()=>({
      arrivalsChecked: !this.state.arrivalsChecked,
    }));
  }
  
  randomStart(side){
    this.props.moves.RandomStart(side);
  }

  onClick(id,opt) {
      console.log(id,opt);
      if( this.state.firstClick===null){  
         this.setState(() => ({
          firstClick: id,
          side: opt,
        }));
        if(this.state.arrivalsChecked && this.state.side===null){
          console.log("calculating arrivals",this.props.G.cells,id);
          var arrivals = pathsFrom(this.props.G.cells,id);
          console.log("arrivées",arrivals)
          this.setState(()=>({
            arrivals: arrivals,
          }));
          //console.log(this.state.arrivals);
        }
      }
      else{
        console.log('first:',this.state.firstClick,'and id:',id)
        if(id ===  this.state.firstClick && opt===this.state.side){
          this.setState(() => ({
            firstClick: null,
            side: null,
            arrivals: new Set(),
          }));
        }
        else{
        if(this.props.ctx.phase==="play"){
            this.props.moves.RegularMove( this.state.firstClick,id);
        }
        else{
            this.props.moves.Place(this.state.side,this.state.firstClick,id);
        }

      }
      this.setState(() => ({
        firstClick: null,
        side: null,
        arrivals: new Set(),
      }));
    }

  }

  render() {
    let winner = '';
    if (this.props.ctx.gameover) {
      winner =
          <div id="winner">Winner: {this.props.ctx.gameover.winner}</div> ;
    }

    const cellStyle = {
      border: '1px solid #555',
      borderRadius: '10px',
      width: '50px',
      height: '50px',
      lineHeight: '0px',
      textAlign: 'center',
    };

    const buttonStyle = {
      margin:'auto',
      width: '60px',
      //height: '30px',
      border: '1px solid black',
    };


    let arrivalNorth = (<tr key="northLine"><td class={appartenance(this.state.arrivals,this.state.firstClick,"north")} style={cellStyle} key="north" onClick={() => this.onClick("north")}>
            {drawCoin(this.props.G["north"])}
          </td></tr>);
    let arrivalSouth = (<tr key="southLine">
                  <td class={appartenance(this.state.arrivals,this.state.firstClick,"south")} style={cellStyle} key="south" onClick={() => this.onClick("south")}>
                  {drawCoin(this.props.G["south"])}
                </td></tr>);
    let tbody = [];
    for (let i = 5; i >=0; i--) {
      let cells = [];
      for (let j = 0; j < 6; j++) {
        const id = 6 * i + j;

        cells.push(
          <td style={cellStyle} class={appartenance(this.state.arrivals,this.state.firstClick,id)}  key={id} onClick={() => this.onClick(id)}>
            {this.props.G.cells[id]===0?"":drawCoin(this.props.G.cells[id])}
          </td>
        );
      }
      tbody.push(<tr key={i}>{cells}</tr>);
    }


    let tNorth = [];
    let cellsN = [];
      for (let j = 0; j < 6; j++) {
        const id =  j;
        cellsN.push(
          <td style={cellStyle} key={id} onClick={() => this.onClick(id,1)}>
            {drawCoin(this.props.G.start[1][id])}
          </td>
        );
      }
      tNorth.push(<tr key="north">{cellsN}</tr>);
    let tSouth = [];
        let cellsS = [];
      for (let j = 0; j < 6; j++) {
        const id =  j;
        cellsS.push(
          <td style={cellStyle} key={id} onClick={() => this.onClick(id,0)}>
            {drawCoin(this.props.G.start[0][id])}
          </td>
        );
      }
      tSouth.push(<tr key="south">{cellsS}</tr>);

      const xdisplay = (this.props.ctx.phase==="init")?"display":"none";
    
    return (
      <div id="main">
        <div class="init-board" class={xdisplay}>
        <table id="north">
<tbody>{tNorth}</tbody>
        </table> 
        <div  style={buttonStyle} onClick={()=>this.randomStart(1)}>Random start</div>
        </div>
        <table>
          <tbody>{arrivalNorth}</tbody>
        </table>
        <table id="board">
          <tbody>{tbody}</tbody>
        </table>
        <table>
          <tbody>{arrivalSouth}</tbody>
        </table>
        <div class="init-board" class={xdisplay}>
        <table id="south">
<tbody>{tSouth}</tbody>
        </table>

        {winner}
        
        <div style={buttonStyle} onClick={()=>this.randomStart(0)}>Random start</div>
        </div>
      </div>
    );
  }
}
