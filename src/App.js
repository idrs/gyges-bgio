import { Client } from 'boardgame.io/react';
import { Gyges } from './Game';
import {GygesBoard} from './Board';

const App = Client({ 
    game: Gyges,
    board: GygesBoard,
 });

export default App;